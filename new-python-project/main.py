"""
Project: new-python-project
Author: Peter Lee
Date: August-21-2017

Description
-----------

"""

import argparse


def main(args):
    """
    Main program.

    Parameters
    ----------
        args : argparse.ArgumentParser.parse_args()
            The program arguments.
    """

    if args.verbose:
        print("verbose is on, let's do something verbosily!")
    elif args.quiet:
        print("quiet is on!")

    print("new-python-project completes successfully.")


if __name__ == "__main__":
    # Parse program arguments
    arguments_parser = argparse.ArgumentParser(description="Please specify the description of the program.")
    arguments_exclusive_groups = arguments_parser.add_mutually_exclusive_group()
    arguments_exclusive_groups.add_argument("-v", "--verbose", help="Display more details in program output", action="store_true")
    arguments_exclusive_groups.add_argument("-q", "--quiet", help="Display only core details in program output", action="store_true")
    arguments_parser.add_argument("-n", "--number", help="this is an example of categorical argument to be parsed into integers", type=int, default=0, choices=[0,1,2,3])
    args = arguments_parser.parse_args()

    main(args)
    